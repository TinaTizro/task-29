import { Component } from '@angular/core';
@Component({
    selector: 'courses',
    template: `
    <h2> {{ title }} </h2>
    <ul>
    <li *ngFor="let course of courses">
        {{ course }}
    </li>
    
    </ul>
    `
})
export class courseComponent {
    title = "List of courses";
    courses = ["Course1", "Course2", "Course3"];

    
}