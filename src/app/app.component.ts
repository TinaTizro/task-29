import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
// export class AppComponent {
//   title = 'my-first-angular!';
// }
export class AppComponent {
  public message: string = 'Hello I am From Typescript!'
  public reminders: string[] = [
    'Learning TypeScript',
    'Learning Angular',
    'Having Fun with Angular'
  ];
  
}